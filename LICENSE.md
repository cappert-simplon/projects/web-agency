

## Footer inspired from:

MDB FREE
Free packages are available under the MIT License.

Hightlights

Free for personal use
Free for commercial use
No attribution required
Copyright notice

Copyright 2020 MDBootstrap.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions.

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

The software is provided "As is", without warranty of any kind, express or implied, including but not limited To the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall The authors or copyright holders be liable for any claim, damages or other liability, whether in an action of Contract, tort or otherwise, arising from, out of or in connection with the software or the use or other Dealings in the software.

## Draw.io / diagrams.net shapes License

The [jgraph / drawio](https://github.com/jgraph/drawio) repo [LICENSE file](https://github.com/jgraph/drawio/blob/dev/LICENSE) contains these statements:

### License
The source code in [the draw.io repo] is licensed under the Apache v2.

The JGraph [draw.io creator] provided icons and diagram templates are licensed under the CC BY 4.0. Additional terms may also apply where the icons are originally defined by a third-party copyright holder. We have checked in all cases that the original license allows use in this project.

Additional minified JavaScript files and Java libraries are used in this project. All of the licenses are deemed compatible with the Apache 2.0, nothing is GPL or AGPL ,due dilgence is performed on all third-party code. [We do not use any of this]



